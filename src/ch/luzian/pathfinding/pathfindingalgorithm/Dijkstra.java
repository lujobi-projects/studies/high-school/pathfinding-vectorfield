/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.pathfindingalgorithm;

import ch.luzian.pathfinding.node.Node;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Dijkstra {

    private final Node nodes[];
    private final HashSet<Node> closedList = new HashSet<>();
    private final HashSet<Node> openList = new HashSet<>();

    public Dijkstra(Node nodes[]) {
        this.nodes = nodes;
    }

    public void calculateDistance(int sourceIndex) {
        Node source = nodes[sourceIndex];
        source.distanceToTarget = 0;
        closedList.add(source);

        addNeighboursToOpenList(source);

        while (!openList.isEmpty()) {
            Node actualNode = findMinNode();
            closedList.add(actualNode);
            openList.remove(actualNode);
            addNeighboursToOpenList(actualNode);
        }

        closedList.clear();
        openList.clear();
    }

    private void addNeighboursToOpenList(Node targetNode) {
        for (HashMap.Entry<Node, Double> node : targetNode.getNeighbors().entrySet()) {
            Node actualNode = node.getKey();

            if (!closedList.contains(actualNode)) {
                openList.add(actualNode);
            }

            double actualNodeDistance = targetNode.distanceToTarget + node.getValue();
            if (actualNode.distanceToTarget > actualNodeDistance) {
                actualNode.distanceToTarget = actualNodeDistance;
                actualNode.setAncestor(targetNode);
            }
        }
    }

    private Node findMinNode() {
        double min = Double.MAX_VALUE;
        Node minNode = null;
        for (Node node : openList) {
            if (node.distanceToTarget < min) {
                min = node.distanceToTarget;
                minNode = node;
            }
        }
        return minNode;
    }

}
