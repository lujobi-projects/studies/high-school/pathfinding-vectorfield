/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.interfaces;

import ch.luzian.pathfinding.node.NodeGrid;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public interface Settings {

//=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#    
    //If you dont know what you are doing only modify code within this sector
    public static NodeGrid NODEGRID = new NodeGrid(35, 26, 30); //Change the appearence of the Nodegrid. First Number: number of Nodes in horizontal direction,
    //Second Number: number of Nodes in vertical direction, Third Number: Resolution of each Node

    //Those Values are optimized for a 1080x1920 Monitor, if you use a different resolution this may be needed to change
    public static final int MAXGRIDWIDTH = 1200; //Size of the Nodegrid in horizontal direction
    public static final int MAXGRIDHEIGHT = 800; //Size of the Nodegrid in vertical direction

//=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#    
    public static final double SCALEX = MAXGRIDWIDTH / NODEGRID.IMAGE_WIDTH;
    public static final double SCALEY = MAXGRIDHEIGHT / NODEGRID.IMAGE_HEIGHT;
}
