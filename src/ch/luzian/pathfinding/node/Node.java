/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.node;

import ch.luzian.pathfinding.graphics.Camera;
import java.util.HashMap;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Node {

    public final int x, y, size;
    public boolean isObstacle;
    public static double INITIAL_DISTANCE = Double.MAX_VALUE / 2;
    public double distanceToTarget = INITIAL_DISTANCE;

    private int color = 0;
    public int pixels[];
    private boolean showGrid = true;
    private boolean drawVector = true;

    private Node ancestor = null;
    private int xAncestorDir = 0, yAncestorDir = 0;
    private final HashMap<Node, Double> neighbors = new HashMap<>();

    public Node(int x, int y, int size, boolean isObstacle) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.isObstacle = isObstacle;
        pixels = new int[size * size];
    }

    public void render(Camera camera) {
        camera.renderNode(this);
    }

    public void setAncestor(Node ancestor) {
        this.ancestor = ancestor;
        xAncestorDir = ancestor.x - x;
        yAncestorDir = ancestor.y - y;
    }

    public int getXAncestorDir() {
        return xAncestorDir;
    }

    public int getYAncestorDir() {
        return yAncestorDir;
    }

    public void setColor(int color) {
        this.color = color;
        recalculatePixels();
    }

    public void showGrid(boolean showGrid) {
        this.showGrid = showGrid;
        recalculatePixels();
    }

    public void drawVector(boolean drawVector) {
        this.drawVector = drawVector;
        recalculatePixels();
    }

    public void addNeighbor(Node neighbor, double distance) {
        neighbors.put(neighbor, distance);
    }

    public HashMap<Node, Double> getNeighbors() {
        return neighbors;
    }

    public void clearNeighbors() {
        neighbors.clear();
    }

    public void reset() {
        distanceToTarget = INITIAL_DISTANCE;
        ancestor = null;
        yAncestorDir = 0;
        xAncestorDir = 0;
    }

    private void recalculatePixels() {
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                if ((y == 0 || x == 0) && showGrid) {
                    pixels[x + y * size] = 0;
                } else {
                    pixels[x + y * size] = color;
                }
            }
        }
        if (drawVector) {
            drawVector();
        }
    }

    private void drawVector() {
        if (ancestor != null) {
            int lastx = 0;
            int lasty = 0;
            int x = size / 2;
            int y = x;
            int distance = size / 10;
            
            while (x > distance && x < size - distance && y > distance && y < size - distance) {
                pixels[x + y * size] = 0;
                lastx = x;
                lasty = y;
                x += xAncestorDir;
                y += yAncestorDir;
            }

            int dirxHead1;
            int dirxHead2;
            int diryHead1;
            int diryHead2;

            if (xAncestorDir == 0) {
                diryHead1 = diryHead2 = -yAncestorDir;
                dirxHead1 = 1;
                dirxHead2 = -1;
            } else if (yAncestorDir == 0) {
                dirxHead1 = dirxHead2 = -xAncestorDir;
                diryHead1 = 1;
                diryHead2 = -1;
            } else {
                dirxHead1 = 0;
                dirxHead2 = -xAncestorDir;
                diryHead1 = -yAncestorDir;
                diryHead2 = 0;
            }

            x = lastx;
            y = lasty;

            for (int i = 0; i < distance; i++) {
                x += dirxHead1;
                y += diryHead1;
                pixels[x + y * size] = 0;
            }

            x = lastx;
            y = lasty;
            for (int i = 0; i < distance; i++) {
                x += dirxHead2;
                y += diryHead2;
                pixels[x + y * size] = 0;
            }
        }
    }
}
