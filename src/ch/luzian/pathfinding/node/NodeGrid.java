/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.node;

import ch.luzian.pathfinding.Pathfinding;
import ch.luzian.pathfinding.graphics.Camera;
import ch.luzian.pathfinding.graphics.RGBPathfindingColor;
import ch.luzian.pathfinding.input.Mouse;
import ch.luzian.pathfinding.pathfindingalgorithm.Dijkstra;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class NodeGrid {

//=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    //If you dont know what you are doing only modify code within this sector
    private static final int MIN_COLOR = 0xFFFFFF;
    private static final int MAX_COLOR = 0x000398;
    private static final int OBSTACLE_COLOR = 0x228B22;
    private static final int VOID_COLOR = 0xFF00FF;

//=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#    
    public final int GRID_WIDTH, GRID_HEIGHT, GRID_SIZE;
    public final int IMAGE_WIDTH, IMAGE_HEIGHT;
    public int pixels[];

    private boolean showGrid = true, allowDiagonalPassthrough = true, drawVectors = true;
    private static final Node DEFAULT_NODE = new Node(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, true);
    private final Node nodes[];
    private final RGBPathfindingColor rgbcolor = new RGBPathfindingColor(MIN_COLOR, MAX_COLOR, OBSTACLE_COLOR, VOID_COLOR);
    private final Dijkstra algorithm;

    public NodeGrid(int gridWidth, int gridHeight, int gridSize) {
        this.GRID_WIDTH = gridWidth;
        this.GRID_HEIGHT = gridHeight;
        this.GRID_SIZE = gridSize;

        nodes = new Node[GRID_HEIGHT * GRID_WIDTH];
        IMAGE_WIDTH = GRID_WIDTH * GRID_SIZE;
        IMAGE_HEIGHT = GRID_HEIGHT * GRID_SIZE;
        pixels = new int[IMAGE_WIDTH * IMAGE_HEIGHT];

        algorithm = new Dijkstra(nodes);

        createNodeList();
        addNeighbors();
    }

    private void createNodeList() {
        for (int y = 0; y < GRID_HEIGHT; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                nodes[x + y * GRID_WIDTH] = new Node(x, y, GRID_SIZE, false);
            }
        }
        setNodesColor(rgbcolor);
    }

    public void render(Camera camera) {
        setNodesColor(rgbcolor);
        for (Node node : nodes) {
            node.showGrid(showGrid);
            node.drawVector(drawVectors);
            node.render(camera);
        }
    }

    private Node oldNode = DEFAULT_NODE;
    private Node oldTargetNode = DEFAULT_NODE;
    private boolean oldDiagonal = allowDiagonalPassthrough;

    public void update() {
        if (oldDiagonal != allowDiagonalPassthrough) {
            reloadNeighbors();
            if (!oldTargetNode.equals(DEFAULT_NODE)) {
                updateGrid(oldTargetNode);
            }
            oldDiagonal = allowDiagonalPassthrough;
        }

        Node targetNode;

        int mouseX = Mouse.getX();
        int mouseY = Mouse.getY();

        if (mouseX >= 0 && mouseY >= 0 && mouseX <= IMAGE_WIDTH * Pathfinding.getScale() && mouseY <= IMAGE_HEIGHT * Pathfinding.getScale()) {
            targetNode = getNodeFromScreen(mouseX, mouseY);

            if (!oldNode.equals(targetNode)) {
                oldNode = targetNode;
                if (Mouse.getButton() == 1 && !targetNode.isObstacle) {
                    oldTargetNode = targetNode;
                    updateGrid(targetNode);

                } else if (Mouse.getButton() == 3) {
                    targetNode.isObstacle = !targetNode.isObstacle;
                    reloadNeighbors();
                    if (!oldTargetNode.equals(DEFAULT_NODE)) {
                        updateGrid(oldTargetNode);
                    }
                }
            }
        }
    }

    public void update(boolean showGrid, boolean allowDiagonalPassthrough, boolean drawVectors) {
        this.showGrid = showGrid;
        this.allowDiagonalPassthrough = allowDiagonalPassthrough;
        this.drawVectors = drawVectors;

        update();
    }

    private Node getNodeFromScreen(int x, int y) {
        return getNodeFromCordinates((int) (x / Pathfinding.getScale()), (int) (y / Pathfinding.getScale()));
    }

    public Node getNodeFromCordinates(int x, int y) {
        int index = (int) x / GRID_SIZE + y / GRID_SIZE * GRID_WIDTH;
        if (index < nodes.length) {
            return nodes[index];
        }
        return null;
    }

    private void calculateAirDistanceToTargetForAllNodes(Node targetNode) {
        for (Node node : nodes) {
            int dx = Math.abs(node.x - targetNode.x);
            int dy = Math.abs(node.y - targetNode.y);
            node.distanceToTarget = Math.sqrt(dx * dx + dy * dy);
        }
    }

    private double findMaxDistanceWithoutDefault() {
        double max = 0;
        for (Node node : nodes) {
            if (node.distanceToTarget > max && !node.isObstacle && node.distanceToTarget < Node.INITIAL_DISTANCE) {
                max = node.distanceToTarget;
            }
        }
        return max;
    }

    private void setNodesColor(RGBPathfindingColor rgbcolor) {
        for (Node node : nodes) {
            if (!node.isObstacle) {
                node.setColor(rgbcolor.getColor(node.distanceToTarget));
            } else {
                node.setColor(rgbcolor.getObstacleColor());
            }
        }
    }

    private void addNeighbors() {
        for (int y = 0; y < GRID_HEIGHT; y++) {
            for (int x = 0; x < GRID_WIDTH; x++) {
                for (int yy = -1; yy < 2; yy++) {
                    for (int xx = -1; xx < 2; xx++) {
                        int x_neighbor = x + xx;
                        int y_neighbor = y + yy;
                        double distance = Math.sqrt(Math.abs(xx) + Math.abs(yy));
                        if (!(x_neighbor < 0 || x_neighbor > GRID_WIDTH - 1 || y_neighbor < 0 || y_neighbor > GRID_HEIGHT - 1 || (xx == 0 && yy == 0) || nodes[x + y * GRID_WIDTH].isObstacle || nodes[x_neighbor + y_neighbor * GRID_WIDTH].isObstacle)) {
                            if ((allowDiagonalPassthrough) || (Math.abs(xx) != Math.abs(yy))) {
                                nodes[x + y * GRID_WIDTH].addNeighbor(nodes[x_neighbor + y_neighbor * GRID_WIDTH], distance);
                            }
                        }
                    }
                }
            }
        }
    }

    private void updateGrid(Node targetNode) {
        for (Node node : nodes) {
            node.reset();
        }
        algorithm.calculateDistance(targetNode.x + targetNode.y * GRID_WIDTH);
        rgbcolor.setColorStep(findMaxDistanceWithoutDefault());
        setNodesColor(rgbcolor);
    }

    private void reloadNeighbors() {
        for (Node node : nodes) {
            node.clearNeighbors();
        }
        addNeighbors();
    }
}
