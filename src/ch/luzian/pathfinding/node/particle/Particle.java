/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.node.particle;

import ch.luzian.pathfinding.graphics.Camera;
import ch.luzian.pathfinding.node.Node;
import ch.luzian.pathfinding.node.NodeGrid;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Particle {

//=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#
    //If you dont know what you are doing only modify code within this sector
    public int[] pixels = new int[]{0xFF00FF, 0x9C2A00, 0x9C2A00, 0x9C2A00, 0xFF00FF, //Here the color of each pixel of the Particle is set. 0xFF00FF will be treated as transparent.
        0x9C2A00, 0xCD8000, 0xFFD700, 0xCD8000, 0x9C2A00,
        0x9C2A00, 0xFFD700, 0xFFD700, 0xFFD700, 0x9C2A00,
        0x9C2A00, 0xCD8000, 0xFFD700, 0xCD8000, 0x9C2A00,
        0xFF00FF, 0x9C2A00, 0x9C2A00, 0x9C2A00, 0xFF00FF};

//=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#=#    
    public static final int BOUNDINGBOX_SIZE = 5;
    private double maxParticleSpeed = 3;
    private double distanceToTarget = Double.MAX_VALUE;
    private double x, y;
    private double sx, sy;
    private double ax = 0.0, ay = 0.0, oldax = 0.0, olday = 0.0;

    private final NodeGrid nodeGrid;
    private Node actualNode;

    public Particle(int x, int y, NodeGrid nodeGrid) {
        this.x = x;
        this.y = y;
        this.nodeGrid = nodeGrid;
        actualNode = nodeGrid.getNodeFromCordinates(x, y);
    }

    public void render(Camera camera) {
        camera.renderParticle((int) Math.round(x), (int) Math.round(y), this);
    }

    public void setAcceleration(double x, double y) {
        oldax = ax;
        olday = ay;
        ax = x;
        ay = y;
    }

    public void setDistanceToTarget(double distanceToTarget) {
        this.distanceToTarget = distanceToTarget;
    }

    public void update() {
        if (distanceToTarget == 0) {
            ax = -oldax;
            ay = -olday;
            oldax = ax;
            olday = ay;
        }
        sx += ax;
        sy += ay;

        if (sx > maxParticleSpeed) {
            sx = maxParticleSpeed;
        }
        if (sy > maxParticleSpeed) {
            sy = maxParticleSpeed;
        }
        if (sx < -maxParticleSpeed) {
            sx = -maxParticleSpeed;
        }
        if (sy < -maxParticleSpeed) {
            sy = -maxParticleSpeed;
        }

        move(sx, sy);
    }

    private void move(double sx, double sy) {

        double xx = x + sx;
        double yy = y + sy;

        if (xx >= 0 && xx < nodeGrid.IMAGE_WIDTH && yy >= 0 && yy < nodeGrid.IMAGE_HEIGHT) {
            Node targetNode = nodeGrid.getNodeFromCordinates((int) xx, (int) yy);
            if (!targetNode.isObstacle || actualNode == targetNode) {
                x = xx;
                y = yy;
                actualNode = targetNode;
            } else {
                if (targetNode.x != actualNode.x) {
                    move(-sx, sy);
                }
                if (targetNode.y != actualNode.y) {
                    move(sx, -sy);
                }
            }
        } else if (xx < 0 || xx >= nodeGrid.IMAGE_WIDTH) {
            move(-sx, sy);
        } else if (yy < 0 || yy >= nodeGrid.IMAGE_HEIGHT) {
            move(sx, -sy);
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setMaxParticleSpeed(double maxParticleSpeed) {
        this.maxParticleSpeed = maxParticleSpeed;
    }

    public Node getActualNode() {
        return actualNode;
    }

}
