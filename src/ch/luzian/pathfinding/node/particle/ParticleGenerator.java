/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.node.particle;

import ch.luzian.pathfinding.graphics.Camera;
import ch.luzian.pathfinding.node.Node;
import ch.luzian.pathfinding.node.NodeGrid;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class ParticleGenerator {

    private final NodeGrid nodeGrid;
    private final HashSet<Particle> particles;

    private int numberOfParticles = 1000;
    private double maxParticleSpeed = 2;
    private double particleAcceleration = 1;

    private final Random random = new Random();

    public ParticleGenerator(NodeGrid nodeGrid) {
        this.nodeGrid = nodeGrid;
        particles = new HashSet<>();
    }

    public void randomDistributeParticles() {
        particles.clear();
        updateParticleSet(numberOfParticles);
    }

    public void setParticleCount(int particleCount) {
        updateParticleSet(particleCount);
    }

    public void update(int numberOfParticles, double maxParticleSpeed, double particleAcceleration) {
        this.numberOfParticles = numberOfParticles;
        this.maxParticleSpeed = maxParticleSpeed;
        this.particleAcceleration = particleAcceleration;
        update();
    }

    public void update() {
        setParticleCount(numberOfParticles);
        for (Particle particle : particles) {

            if (particle.getActualNode().isObstacle) {
                particles.remove(particle);
                break;
            }

            Node nodeUnderParticle = nodeGrid.getNodeFromCordinates((int) particle.getX(), (int) particle.getY());
            double accx = ((double) nodeUnderParticle.getXAncestorDir()) * particleAcceleration;
            double accy = ((double) nodeUnderParticle.getYAncestorDir()) * particleAcceleration;
            
            particle.setMaxParticleSpeed(maxParticleSpeed);
            particle.setAcceleration(accx, accy);
            particle.setDistanceToTarget(nodeUnderParticle.distanceToTarget);
            particle.update();
        }
    }

    public void render(Camera camera) {
        for (Particle particle : particles) {
            particle.render(camera);
        }
    }

    private void updateParticleSet(int particleCount) {
        while (particles.size() < particleCount) {
            int x = random.nextInt(nodeGrid.IMAGE_WIDTH);
            int y = random.nextInt(nodeGrid.IMAGE_HEIGHT);
            if (!nodeGrid.getNodeFromCordinates(x, y).isObstacle) {
                particles.add(new Particle(x, y, nodeGrid));
            }
        }
        while (particles.size() > particleCount) {
            Iterator<Particle> iter = particles.iterator();
            particles.remove(iter.next());
        }
    }
}
