/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding;

import ch.luzian.pathfinding.node.NodeGrid;
import ch.luzian.pathfinding.graphics.Camera;
import ch.luzian.pathfinding.input.Mouse;
import ch.luzian.pathfinding.interfaces.Settings;
import ch.luzian.pathfinding.node.particle.ParticleGenerator;
import java.awt.Canvas;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Pathfinding extends Canvas implements Runnable, ActionListener, Settings {

    private static final String TITLE = "Pathfinding - by Luzian Bieri v1.1";
    private static final String PATH = "/res/icon.PNG";
    private static double SCALE;

    private NodeGrid nodegrid;
    private ParticleGenerator particleGenerator;

    private static JCheckBox showGridCheckbox;
    private static JCheckBox allowDiagonalCheckbox;
    private static JCheckBox drawVectorsCheckbox;
    private static JSlider numberOfParticlesSlider;
    private static JLabel numberOfParticlesValueLabel;
    private static JLabel numberOfParticlesLabel;
    private static JSlider maxParticleSpeedSlider;
    private static JLabel maxParticleSpeedValueLabel;
    private static JLabel maxParticleSpeedLabel;
    private static JSlider particleAccelerationSlider;
    private static JLabel particleAccelerationValueLabel;
    private static JLabel particleAccelerationLabel;
    private static JButton redrawParticles;
    private static JLabel titleLabel;
    private static JTextArea manualField;
    private static Font font;
    private DecimalFormat decimalFormat;

    private Thread gameThread;
    private boolean running;

    private JFrame frame;
    private Camera camera;

    private BufferedImage image;
    private int[] pixels;

    private boolean regenerateParticles = false;
    private static boolean error = false;

    public static void main(String[] args) {
        Pathfinding pathfinding = new Pathfinding();
        if (!error) {
            try {
                pathfinding.frame.setIconImage(ImageIO.read(Pathfinding.class.getResource(PATH)));
            } catch (IOException ex) {
            }
            pathfinding.frame.setResizable(false);
            pathfinding.frame.setTitle(TITLE);
            Container container = pathfinding.frame.getContentPane();

            addComponentsToContainer(container, pathfinding);

            pathfinding.frame.pack();

            pathfinding.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            pathfinding.frame.setLocationRelativeTo(null);
            pathfinding.frame.setVisible(true);

            pathfinding.start();
        }
    }

    public Pathfinding() {
        SCALE = Math.min(SCALEX, SCALEY);
        if (SCALE < 1) {
            JOptionPane.showMessageDialog(this, "The NodeGrid too big. Please set a smaller Resolution or set a smaller Size of the Grid", "NodeGrid too big", JOptionPane.ERROR_MESSAGE);
            error = true;
        } else {
            init();
        }
    }

    @Override
    public void run() {
        long lastTime = System.nanoTime();
        long timer = System.currentTimeMillis();
        final double ns = 1000000000.0 / 60.0;
        double delta = 0;
        int frames = 0;
        int ticks = 0;

        requestFocus();
        
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            
            while (delta >= 1) {
                tick();
                ticks++;
                delta--;
            }
            
            render();
            frames++;
            
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                frame.setTitle(TITLE + " | " + ticks + " tps, " + frames + " fps");
                System.out.println(ticks + " tps, " + frames + " fps");
                frames = 0;
                ticks = 0;
            }
        }
    }

    public synchronized void start() {
        running = true;
        gameThread = new Thread(this, "Pathfinding");
        gameThread.start();
    }

    public synchronized void stop() {
        running = false;
        try {
            gameThread.join();
        } catch (InterruptedException ex) {
        }
    }

    private void tick() {
        int numberOfParticles = calculateNumbersOfParticles(numberOfParticlesSlider.getValue(), numberOfParticlesSlider.getMaximum());
        double maxParticleSpeed = (double) maxParticleSpeedSlider.getValue() / 10;
        double particleAcceleration = (double) calculateparticleAcceleration(particleAccelerationSlider.getValue(), particleAccelerationSlider.getMaximum());

        numberOfParticlesValueLabel.setText(String.valueOf(numberOfParticles));
        maxParticleSpeedValueLabel.setText(String.valueOf(maxParticleSpeed));
        particleAccelerationValueLabel.setText(decimalFormat.format(particleAcceleration));

        if (regenerateParticles) {
            particleGenerator.randomDistributeParticles();
            regenerateParticles = false;
        }

        particleGenerator.update(numberOfParticles, maxParticleSpeed, particleAcceleration);
        nodegrid.update(showGridCheckbox.isSelected(), allowDiagonalCheckbox.isSelected(), drawVectorsCheckbox.isSelected());
    }

    private void render() {
        BufferStrategy bs = getBufferStrategy();
        if (bs == null) {
            createBufferStrategy(3);
            return;
        }

        nodegrid.render(camera);
        particleGenerator.render(camera);

        System.arraycopy(camera.pixels, 0, pixels, 0, pixels.length);

        Graphics g = bs.getDrawGraphics();
        g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
        g.dispose();
        bs.show();
    }

    private void init() {
        this.nodegrid = NODEGRID;
        particleGenerator = new ParticleGenerator(nodegrid);

        Mouse mouse = new Mouse();

        decimalFormat = new DecimalFormat("#.###");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);

        frame = new JFrame();
        showGridCheckbox = new JCheckBox("Show grid", true);
        allowDiagonalCheckbox = new JCheckBox("Allow diagonal Passthrough", true);
        drawVectorsCheckbox = new JCheckBox("Draw Vectors", true);
        numberOfParticlesSlider = new JSlider(0, 100, 0);
        numberOfParticlesValueLabel = new JLabel();
        numberOfParticlesLabel = new JLabel("Number of Particles");
        maxParticleSpeedSlider = new JSlider(0, 60, 10);
        maxParticleSpeedValueLabel = new JLabel();
        maxParticleSpeedLabel = new JLabel("Max. Particle-Speed");
        particleAccelerationSlider = new JSlider(0, 2731, 1000);
        particleAccelerationValueLabel = new JLabel();
        particleAccelerationLabel = new JLabel("Max. Particle-Acceleration");
        redrawParticles = new JButton("Place Particles at Random Location");
        titleLabel = new JLabel("Pathfinding - Luzian Bieri v1.1");
        manualField = new JTextArea("This example of Pathfinding uses the Dijkstra-Algorithm to create a field of Direction-Vectors. Use the left mouse-button to set the target and the right to set obstacles. For both the mouse can be dragged too. 'Draw Vectors', 'Show Grid' and 'Number of Particles' are self-explainitory 'Allow diagonal Passthrough' means wheather acceleration in 45° direction, and therefore passing between two diagonal adjacent nodes, should be allowed or not. 'Particle-Speed' and 'Particle-Acceleration' are mesured in pixels/tick resp. pixels/tick/tick. Average ticks per second is 60 and default-pixels (if not changed in the Code) 30 pixels per field. Actual frames per second and ticks per second can be seen in the title of this window. FPS highly depend on the amount of generated particles. Unrechable nodes (or if no target is selected) are indicated with magenta. If not changed in the code the darker the color is the further away the node is from its target.");
        font = new Font("Verdana", Font.BOLD, 13);

        showGridCheckbox.setFont(font);
        allowDiagonalCheckbox.setFont(font);
        drawVectorsCheckbox.setFont(font);
        numberOfParticlesValueLabel.setFont(font);
        numberOfParticlesLabel.setFont(font);
        maxParticleSpeedValueLabel.setFont(font);
        maxParticleSpeedLabel.setFont(font);
        particleAccelerationValueLabel.setFont(font);
        particleAccelerationLabel.setFont(font);
        redrawParticles.setFont(font);
        manualField.setFont(font);
        titleLabel.setFont(new Font("Verdana", Font.BOLD, 35));

        manualField.setEditable(false);
        manualField.setLineWrap(true);
        numberOfParticlesValueLabel.setHorizontalAlignment(JTextField.HORIZONTAL);
        maxParticleSpeedValueLabel.setHorizontalAlignment(JTextField.HORIZONTAL);
        particleAccelerationValueLabel.setHorizontalAlignment(JTextField.HORIZONTAL);
        redrawParticles.addActionListener(this);

        camera = new Camera(nodegrid.IMAGE_WIDTH, nodegrid.IMAGE_HEIGHT);

        image = new BufferedImage(nodegrid.IMAGE_WIDTH, nodegrid.IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
        pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

        Dimension size = new Dimension((int) (nodegrid.IMAGE_WIDTH * SCALE), (int) (nodegrid.IMAGE_HEIGHT * SCALE));
        setSize(size);

        addMouseListener(mouse);
        addMouseMotionListener(mouse);

    }

    private static void addComponentsToContainer(Container container, Pathfinding pathfinding) {
        container.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(10, 10, 10, 10);
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 110;
        container.add(pathfinding, c);

        c.gridheight = 1;

        c.gridwidth = 6;
        c.gridx = 1;
        c.gridy = 0;
        container.add(titleLabel, c);

        c.gridx = 1;
        c.gridy = 1;
        container.add(manualField, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 2;
        container.add(drawVectorsCheckbox, c);

        c.gridx = 3;
        c.gridy = 2;
        container.add(showGridCheckbox, c);

        c.gridx = 5;
        c.gridy = 2;
        container.add(allowDiagonalCheckbox, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 3;
        container.add(numberOfParticlesLabel, c);

        c.gridwidth = 3;
        c.gridx = 3;
        c.gridy = 3;
        container.add(numberOfParticlesSlider, c);

        c.gridwidth = 1;
        c.gridx = 6;
        c.gridy = 3;
        container.add(numberOfParticlesValueLabel, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 4;
        container.add(maxParticleSpeedLabel, c);

        c.gridwidth = 3;
        c.gridx = 3;
        c.gridy = 4;
        container.add(maxParticleSpeedSlider, c);

        c.gridwidth = 1;
        c.gridx = 6;
        c.gridy = 4;
        container.add(maxParticleSpeedValueLabel, c);

        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 5;
        container.add(particleAccelerationLabel, c);

        c.gridwidth = 3;
        c.gridx = 3;
        c.gridy = 5;
        container.add(particleAccelerationSlider, c);

        c.gridwidth = 1;
        c.gridx = 6;
        c.gridy = 5;
        container.add(particleAccelerationValueLabel, c);

        c.insets = new Insets(25, 25, 25, 25);
        c.gridwidth = 6;
        c.gridx = 1;
        c.gridy = 7;
        container.add(redrawParticles, c);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(redrawParticles)) {
            regenerateParticles = true;
        }
    }

    private int calculateNumbersOfParticles(int value, int max) {
        double x = (double) value / (double) max;
        //return (int) Math.round(50722.435 * Math.pow(30.0, 0.25*((x*5.0)-5.0)) -722.439);
        return (int) Math.round(50000 * Math.pow(30.0, 0.65 * ((x * 5.0) - 5.0)) - 0.3);
    }

    private double calculateparticleAcceleration(int value, int max) {
        double x = (double) value / (double) max;
        return 5 * Math.pow(100, 0.65 * (x - 1.0)) - 0.25;
    }

    public static double getScale() {
        return SCALE;
    }
}
