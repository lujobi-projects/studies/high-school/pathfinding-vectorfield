/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.graphics;

import ch.luzian.pathfinding.node.Node;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class RGBPathfindingColor {

    private int minRed, minGreen, minBlue;
    private int maxRed, maxGreen, maxBlue;
    private final int obstacleColor, unreachableColor;
    private double disRed, disGreen, disBlue;

    public RGBPathfindingColor(int minColor, int maxColor, int obstacleColor, int voidColor) {
        setMaxColor(maxColor);
        setMinColor(minColor);
        this.obstacleColor = obstacleColor;
        this.unreachableColor = voidColor;
        setColorStep(Node.INITIAL_DISTANCE);
    }

    public void setMinColor(int minColor) {
        minRed = minColor / 256 / 256;
        minGreen = minColor / 256 % 256;
        minBlue = minColor % 256;
    }

    public void setMaxColor(int maxColor) {
        maxRed = maxColor / 256 / 256;
        maxGreen = maxColor / 256 % 256;
        maxBlue = maxColor % 256;
    }

    public void setColorStep(double maxColorDistance) {
        disRed = (maxRed - minRed) / maxColorDistance;
        disGreen = (maxGreen - minGreen) / maxColorDistance;
        disBlue = (maxBlue - minBlue) / maxColorDistance;
    }

    public int getObstacleColor() {
        return obstacleColor;
    }

    public int getUnreachableColor() {
        return unreachableColor;
    }

    public int getColor(double step) {
        if (step == Node.INITIAL_DISTANCE) {
            return getUnreachableColor();
        }
        
        int red = (int) (minRed + disRed * step);
        int green = (int) (minGreen + disGreen * step);
        int blue = (int) (minBlue + disBlue * step);

        int color = (int) (red * 256 * 256 + green * 256 + blue);
        return color;
    }
}
