/* 
 * Copyright (C) 2016 Luzian Bieri <l.j.bieri@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package ch.luzian.pathfinding.graphics;

import ch.luzian.pathfinding.node.Node;
import ch.luzian.pathfinding.node.particle.Particle;

/**
 *
 * @author Luzian Bieri <l.j.bieri@gmail.com>
 */
public class Camera {

    private final int width, height;
    public int[] pixels;

    public Camera(int width, int height) {
        this.width = width;
        this.height = height;
        pixels = new int[width * height];
    }

    public void clear() {
        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = 0;
        }
    }

    public void renderNode(Node node) {
        for (int y = 0; y < node.size; y++) {
            for (int x = 0; x < node.size; x++) {
                pixels[(node.x * node.size) + x + ((node.y * node.size + y) * width)] = node.pixels[x + y * node.size];
            }
        }
    }

    int boundingBoxOffset = Particle.BOUNDINGBOX_SIZE / 2;

    public void renderParticle(int x, int y, Particle particle) {
        for (int yy = 0; yy < Particle.BOUNDINGBOX_SIZE; yy++) {
            for (int xx = 0; xx < Particle.BOUNDINGBOX_SIZE; xx++) {
                int actualX = x + xx - boundingBoxOffset;
                int actualY = y + yy - boundingBoxOffset;
                
                if (actualX >= 0 && actualY >= 0 && actualX < width && actualY < height) {
                    int color = particle.pixels[xx + yy * Particle.BOUNDINGBOX_SIZE];
                    if (color != 0xFF00FF) {
                        pixels[actualX + actualY * width] = color;
                    }
                }
            }
        }
    }

    public void update() {
        //Camera UpdateCode goes here
    }
}
